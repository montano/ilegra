package com.ilegra.domain;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import au.com.bytecode.opencsv.CSVWriter;

import com.ilegra.core.FactoryDataFile;

public class DomainResults {

	/**
	 * Amount sum clients
	 */
	private int sumClients;

	/**
	 * Amount sum salesman
	 */
	private int sumSalesman;

	/**
	 * ID of bigest sale
	 */
	private int idExpensiveSale;

	/**
	 * Last bigest sales
	 */
	private double lastGreatTotalSales;		
	
	/**
	 * Total sales for each salesman
	 */
	private HashMap<String, Double> salesmanSumSales = new HashMap<String, Double>();

	public int getSumCostumers() {
		return sumClients;
	}

	public int getSumSalesman() {
		return sumSalesman;
	}

	public int getIdExpensiveSale() {
		return idExpensiveSale;
	}

	public String getWorstSalesman() {

		String lastWorstSalesman = null;
		double lastValue = -1;
		
		for(Entry<String, Double> entry : salesmanSumSales.entrySet()) {
						
		    String lastDummySalesman = entry.getKey();
		    Double value = entry.getValue();
		    
		    if (value < lastValue || lastValue == -1) {
		    	lastWorstSalesman = lastDummySalesman;
		    }
		    lastValue = value;
		}
		
		return lastWorstSalesman;
	}

	/**
	 * Proccessing informations of amount.
	 * 
	 * @param domain
	 *            Last domain imported
	 */
	public void done(DomainDataFile domain) {

		switch (domain.getType()) {
		case FactoryDataFile.TYPE_COSTUMER:
			sumClients++;
			break;
		case FactoryDataFile.TYPE_SALESMAN:
			sumSalesman++;
			break;
		case FactoryDataFile.TYPE_SALES:
			
			DomainSales domainSales = (DomainSales) domain;
			double totalSales = domainSales.getTotalSales();
			
			if (totalSales > lastGreatTotalSales) {
				lastGreatTotalSales = totalSales;
				idExpensiveSale = domainSales.getId();
			}
			
			String name = domainSales.getSalesman().getName();
			double atual = 0;
			if (salesmanSumSales.containsKey(name)) {
				atual = salesmanSumSales.get(name);
			}
			salesmanSumSales.put(name, atual + totalSales);
			
			break;
		}
	}
	
	/**
	 * Save result in path.
	 * 
	 * @param name Name of result file
	 * @param dir Dir of result file
	 * @throws IOException
	 */
	public void saveResult(String name, String dir) throws IOException {
		
		String finalDir = new StringBuilder(dir).append("/").append(name).append(".done").append(".dat").toString();
		
		CSVWriter writer = new CSVWriter(new FileWriter(finalDir), DomainDataFile.DELIMETER);
		
		String[] data = new String[] {
				String.valueOf(sumClients), 
				String.valueOf(sumSalesman), 
				String.valueOf(idExpensiveSale), 
				getWorstSalesman()
			};
		
		writer.writeNext(data);
		writer.close();
	}
}
