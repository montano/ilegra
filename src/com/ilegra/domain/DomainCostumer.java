package com.ilegra.domain;


public class DomainCostumer extends DomainPerson {

	private static final int POSITION_CNPJ = 1;	
	private static final int POSITION_BUSINESS = 3;
	
	/**
	 * Salesman CPF
	 */
	private String cnpj;
	
	/**
	 * Salesman business
	 */
	private String business;
		
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getBusiness() {
		return business;
	}
	
	public void setBusiness(String business) {
		this.business = business;
	}

	/**
	 * Parse row to Domain.
	 * 
	 * @param row Array of values
	 */
	public void parse(String[] row) {
		if (checkValues(row)) {
			super.parse(row);
			setCnpj(row[POSITION_CNPJ]);
			setName(row[POSITION_NAME]);
			setBusiness(row[POSITION_BUSINESS]);
		}
	}
	
	/**
	 * Check if values are alright.
	 * 
	 * @param row Array of values
	 * @return {@link Boolean} TRUE if success
	 */
	public boolean checkValues(String[] row) {
		
		if (super.checkValues(row)) {
		
			/**
			 * Validation CPF
			 */
			if (row[POSITION_CNPJ] == null) {
				return false;
			}	
			
			/**
			 * Validation Business
			 */
			if (row[POSITION_BUSINESS] == null) {
				return false;
			}
		} else {
			return false;
		}
		
		return true;
	}
}