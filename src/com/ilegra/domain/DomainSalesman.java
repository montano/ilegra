package com.ilegra.domain;

import java.util.regex.Pattern;


public class DomainSalesman extends DomainPerson {

	private static final int POSITION_CPF = 1;
	private static final int POSITION_SALARY = 3;
	
	/**
	 * Salesman CPF
	 */
	private String cpf;
	
	/**
	 * Salesman salary
	 */
	private Double salary;
		
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Double getSalary() {
		return salary;
	}
	
	public void setSalary(Double salary) {
		this.salary = salary;
	}

	/**
	 * Parse row to Domain.
	 * 
	 * @param row Array of values
	 */
	public void parse(String[] row) {
		if (checkValues(row)) {
			super.parse(row);
			setCpf(row[POSITION_CPF]);
			setName(row[POSITION_NAME]);
			setSalary(Double.valueOf(row[POSITION_SALARY]));
		}
	}
	
	/**
	 * Check if values are alright.
	 * 
	 * @param row Array of values
	 * @return {@link Boolean} TRUE if success
	 */
	public boolean checkValues(String[] row) {
		
		if (super.checkValues(row)) {
			
			/**
			 * Validation CPF
			 */
			if (row[POSITION_CPF] == null) {
				return false;
			}
			
			/**
			 * Validation Salary
			 */
			if (row[POSITION_SALARY] == null) {
				return false;
			}
			
			Pattern p = Pattern.compile("\\d+(\\.\\d+)?");
			if ( !p.matcher(row[POSITION_SALARY]).find()) {
				return false;
			}			
		
		} else {
			return false;
		}
		
		return true;
	}
}