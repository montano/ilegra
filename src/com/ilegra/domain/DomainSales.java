package com.ilegra.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

public class DomainSales extends DomainDataFile {
	
	private static final int POSITION_ID = 1;
	private static final int POSITION_ITENS = 2;
	private static final int POSITION_SALESNAME = 3;
	
	/**
	 * Sales ID
	 */
	private int id;
	
	/**
	 * Salesman of this sales
	 */
	private DomainSalesman salesman;
	
	/**
	 * Itens sales
	 */
	private ArrayList<DomainItem> itens;
	
	/**
	 * Sales Total
	 */
	private double totalSales;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public DomainSalesman getSalesman() {
		return salesman;
	}

	public void setSalesman(String salesname) {
		DomainSalesman domain = new DomainSalesman();
		domain.setName(salesname);
		this.salesman = domain;
	}
	
	public ArrayList<DomainItem> getItens() {
		return itens;
	}

	public void setItens(ArrayList<DomainItem> itens) {
		this.itens = itens;
	}
	
	@Override
	public void parse(String[] row) {
		if (checkValues(row)) {
			setType(Integer.valueOf(row[POSITION_TYPE]));
			setId(Integer.valueOf(row[POSITION_ID]));
			setSalesman(row[POSITION_SALESNAME]);
			importItens(row[POSITION_ITENS]);
		}
	}

	/**
	 * Import itens to sales.
	 * 
	 * @param rowItens CSV String
	 */
	private void importItens(String rowItens) {
		
		/**
		 * List of itens
		 */
		ArrayList<DomainItem> itens = new ArrayList<DomainItem>();
				
		/**
		 * Split any itens
		 */
		String[] items = new Gson().fromJson(rowItens, String[].class);
		
		/**
		 * For each row of itens
		 */
		for (String itemRow : items) {
			
			/**
			 * Split informations
			 */
			List<String> item = Arrays.asList(itemRow.split("\\s*" + DomainItem.DELIMETER + "\\s*"));
			
			/**
			 * Parse informations to domain bean
			 */
			String[] row = item.toArray(new String[item.size()]);
			DomainItem domain = new DomainItem();
		    domain.parse(row);
		    
		    totalSales += domain.getPrice() * domain.getNumber();
		    
		    itens.add(domain);
		}
		
		/**
		 * Set list itens on sales
		 */
		setItens(itens);
	}

	@Override
	public boolean checkValues(String[] row) {
		
		if (row.length < 4) {
			return false;
		}
		
		if (row[POSITION_ID] == null) {
			return false;
		}
		
		if ( ! row[POSITION_ID].matches("\\d+")) {
			return false;
		}
		
		if (row[POSITION_SALESNAME] == null) {
			return false;
		}
		
		if (row[POSITION_ITENS] == null) {
			return false;
		}
		
		return true;
	}

	public double getTotalSales() {
		return totalSales;
	}
}