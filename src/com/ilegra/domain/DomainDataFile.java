package com.ilegra.domain;

public abstract class DomainDataFile {

	/**
	 * Position in CSV
	 */
	protected static final int POSITION_TYPE = 0;
	
	/**
	 * CSV delimiter.
	 */
	public static final char DELIMETER = 'ç';
	
	/**
	 * Type of data
	 * 
	 * 1 - Salesman
	 * 2 - Costumer
	 * 3 - Sales
	 */
	private int type;

	/**
	 * Type of data
	 * 
	 * @return int codigo do tipo.
	 */
	public int getType() {
		return type;
	}

	/**
	 * Set type of data
	 * 
	 * @param type Tipo do arquivo
	 */
	protected void setType(int type) {
		this.type = type;
	}
	
	public abstract void parse(String[] row);
	public abstract boolean checkValues(String[] row);
}
