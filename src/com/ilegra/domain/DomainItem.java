package com.ilegra.domain;

import java.util.regex.Pattern;

public class DomainItem extends DomainDataFile {

	/**
	 * CSV delimiter.
	 */
	public static final char DELIMETER = '-';	
	
	private static final int POSITION_ID = 0;
	private static final int POSITION_NUMBER = 1;
	private static final int POSITION_PRICE = 2;
	
	/**
	 * Item ID
	 */
	private int id;	
	
	/**
	 * Quantity of itens
	 */
	private int number;
	
	/**
	 * Unity Price
	 */
	private double price;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public void parse(String[] row) {
		if (checkValues(row)) {
			setId(Integer.valueOf(row[POSITION_ID]));
			setNumber(Integer.valueOf(row[POSITION_NUMBER]));
			setPrice(Double.valueOf(row[POSITION_PRICE]));
		}
	}

	@Override
	public boolean checkValues(String[] row) {
		
		if (row.length < 3) {
			return false;
		}
		
		if (row[POSITION_ID] == null) {
			return false;
		}
		
		if ( ! row[POSITION_ID].matches("\\d+")) {
			return false;
		}
		
		if (row[POSITION_NUMBER] == null) {
			return false;
		}
		
		if ( ! row[POSITION_NUMBER].matches("\\d+")) {
			return false;
		}
		
		if (row[POSITION_PRICE] == null) {
			return false;
		}
		
		Pattern p = Pattern.compile("\\d+(\\.\\d+)?");
		if ( !p.matcher(row[POSITION_PRICE]).find()) {
			return false;
		}	
		
		return true;
	}
}