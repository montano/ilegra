package com.ilegra.domain;

public abstract class DomainPerson extends DomainDataFile {
	
	protected static final int POSITION_NAME = 2;
	
	/**
	 * Person name
	 */
	private String name;

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Parse row to Domain.
	 * 
	 * @param row Array of values
	 */
	public void parse(String[] row) {
		if (checkValues(row)) {
			setType(Integer.valueOf(row[POSITION_TYPE]));
			setName(row[POSITION_NAME]);			
		}
	}
	
	public boolean checkValues(String[] row) {
		/**
		 * Validation Name
		 */
		if (row[POSITION_NAME] == null) {
			return false;
		}
		
		return true;
	}
}
