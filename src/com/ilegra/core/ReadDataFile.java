package com.ilegra.core;

import com.ilegra.domain.DomainDataFile;
import com.ilegra.domain.DomainSalesman;

public class ReadDataFile {

	/**
	 * Read a row information about some {@link DomainDataFile}.
	 * 
	 * @param row Row to be read
	 * @return {@link Boolean} TRUE if success
	 */
	public DomainDataFile readRow(String[] row) {		
		DomainSalesman domain = new DomainSalesman();
		domain.parse(row);
		return domain;
	}
}
