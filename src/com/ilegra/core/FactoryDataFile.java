package com.ilegra.core;

import com.ilegra.domain.DomainCostumer;
import com.ilegra.domain.DomainDataFile;
import com.ilegra.domain.DomainSales;
import com.ilegra.domain.DomainSalesman;

/**
 * 
 * <b>Objective:</b> Return a validated Domain
 * 
 * @author lucas
 *
 */
public class FactoryDataFile {
	
	private final static FactoryDataFile instance = new FactoryDataFile();

	private static final int TYPE_POSITION = 0;
	
	public static final int TYPE_SALESMAN = 1;
	public static final int TYPE_COSTUMER = 2;
	public static final int TYPE_SALES = 3;

	private FactoryDataFile() {
		
	}
	
	public static FactoryDataFile getInstance() {
		return instance;
	}
	
	/**
	 * Return a Domain to import some specific information
	 * 
	 * @param row String[]
	 * @return {@link DomainDataFile} Domain to be used by CSV Reader
	 */
	public DomainDataFile getDomain(String[] row) {
		
		DomainDataFile domain = null;
		
		
		if (checkType(row)) {
			
			switch (Integer.valueOf(row[TYPE_POSITION])) {
			case TYPE_SALESMAN:
				domain = new DomainSalesman();
				break;
			case TYPE_COSTUMER:
				domain = new DomainCostumer();
				break;
			case TYPE_SALES:
				domain = new DomainSales();
				break;
			}
		}
		
		return domain;
	}

	/**
	 * Check type information
	 * 
	 * @param row
	 * @return {@link Boolean} TRUE if success
	 */
	public boolean checkType(String[] row) {
		
		/**
		 * Verificamos se a linha nao esta em branco.
		 */
		if (row == null || row.length == 0) {
			return false;
		}
		
		/**
		 * Verificamos se o primeiro se trata de um numero.
		 */
		if ( ! row[TYPE_POSITION].matches("\\d+")) {
			return false;	
		}
		
		/**
		 * Verificamos se a primeira se trata de um numero valido
		 */
		if ( ! Integer.valueOf(row[TYPE_POSITION]).toString().matches("[1-3]")) {
			return false;
		}
		
		return true;
	}
}
