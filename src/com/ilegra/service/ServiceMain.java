package com.ilegra.service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import com.ilegra.core.ImportFile;
import com.ilegra.domain.DomainResults;

public class ServiceMain {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

//		String dirIn = "/Users/lucas/Downloads/ilegra/in";
//		String dirOut = "/Users/lucas/Downloads/ilegra/out";
		
		String dirIn = "C:\\data\\in";
		String dirOut = "C:\\dados\\out";

		WatchService watcher = FileSystems.getDefault().newWatchService();

		Path dir = Paths.get(dirIn);
		try {
			dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);
		} catch (IOException x) {
			System.err.println(x);
		}

		for (;;) {

			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				if (kind == StandardWatchEventKinds.OVERFLOW) {
					continue;
				}

				
				Path dirPath = (Path)key.watchable();
				Path fullPath = dirPath.resolve((Path) event.context());
				

				ImportFile importer = new ImportFile();
				
				if (importer.checkExtension(fullPath.toString())) {
					importer.setDir(fullPath.toString());
					DomainResults result = importer.importFile();
					result.saveResult(fullPath.toFile().getName().toString().replace(".dat", ""), dirOut);
				}
			}
			boolean valid = key.reset();
			if (!valid) {
				break;
			}

		}
	}
}
