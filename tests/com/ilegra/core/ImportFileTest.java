package com.ilegra.core;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.domain.DomainCostumer;
import com.ilegra.domain.DomainDataFile;
import com.ilegra.domain.DomainResults;
import com.ilegra.domain.DomainSales;
import com.ilegra.domain.DomainSalesman;

public class ImportFileTest {

	private ImportFile instance;

	@Before
	public void setUp() throws Exception {
		instance = new ImportFile();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testCheckExtension() {
		try {
			Assert.assertTrue("Worng Extension, only files with .dat extension", instance.checkExtension(FactoryDataFileTest.FILE_DIR_TYPE_1));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testImport() {
		try {
			instance.setDir(FactoryDataFileTest.FILE_DIR_TYPE);			
			DomainResults results = instance.importFile();
			Assert.assertEquals(10, results.getIdExpensiveSale());
			Assert.assertEquals(3, results.getSumCostumers());
			Assert.assertEquals(2, results.getSumSalesman());
			Assert.assertEquals("Renato", results.getWorstSalesman());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testSaveResults() {
		try {
			instance.setDir(FactoryDataFileTest.FILE_DIR_TYPE);			
			DomainResults results = instance.importFile();
			
			File file = new File(FactoryDataFileTest.FILE_DIR_TYPE);
			results.saveResult(file.getName().replace(".dat", ""), file.getParent() + "/out");			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testGetLastDataFiles() {
		try {
			instance.setDir(FactoryDataFileTest.FILE_DIR_TYPE);			
			instance.importFile();
			
			ArrayList<DomainDataFile> dataFile = instance.getLastDataFiles();
			Assert.assertNotNull("Import fail", dataFile);
			Assert.assertTrue("Import fail", dataFile.size() > 0);
			
			DomainDataFile domainSalesman = dataFile.get(0);
			Assert.assertTrue(domainSalesman instanceof DomainSalesman);
			
			DomainDataFile domainSalesman1 = dataFile.get(1);
			Assert.assertTrue(domainSalesman1 instanceof DomainSalesman);
			
			DomainDataFile domainCostumer = dataFile.get(2);
			Assert.assertTrue(domainCostumer instanceof DomainCostumer);
			
			DomainSales domainSales = (DomainSales) dataFile.get(5);
			Assert.assertTrue(domainSales instanceof DomainSales);
			
			Assert.assertEquals(10, domainSales.getId());
			Assert.assertEquals(3, domainSales.getItens().size());
			Assert.assertEquals(((DomainSalesman) domainSalesman).getName(), domainSales.getSalesman().getName());
			Assert.assertTrue(3.10D == domainSales.getItens().get(2).getPrice());
			
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
