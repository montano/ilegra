package com.ilegra.core;

import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.ilegra.domain.DomainCostumer;
import com.ilegra.domain.DomainDataFile;
import com.ilegra.domain.DomainSales;
import com.ilegra.domain.DomainSalesman;

public class FactoryDataFileTest {

	public static final String FILE_DIR_TYPE_1 = "assets/tests/file_type_1.dat";
	public static final String FILE_DIR_TYPE_2 = "assets/tests/file_type_2.dat";
	public static final String FILE_DIR_TYPE_3 = "assets/tests/file_type_3.dat";
	public static final String FILE_DIR_ITEM = "assets/tests/file_type_item.dat";
	public static final String FILE_DIR_TYPE = "assets/tests/file_type.dat";	

	/**
	 * Test if return of Factory is instance of DomainFile.
	 * {@link DomainDataFile}
	 */
	@Test
	public void testGetDomain() {

		DomainDataFile decorator = null;
		
		try {
			
			String[] row = ImportFile.getCSVReader(FILE_DIR_TYPE_1).readNext();
			decorator = FactoryDataFile.getInstance().getDomain(row);

			Assert.assertTrue("Factory do not return a Domain", decorator instanceof DomainDataFile);			
			
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test if is a valid type.
	 */
	@Test
	public void testCheckType() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FILE_DIR_TYPE_1).readNext();
			Assert.assertTrue("Type not found or invalid", FactoryDataFile.getInstance().checkType(row));
			
			String[] row2 = ImportFile.getCSVReader(FILE_DIR_TYPE_2).readNext();
			Assert.assertTrue("Type not found or invalid", FactoryDataFile.getInstance().checkType(row2));
			
			String[] row3 = ImportFile.getCSVReader(FILE_DIR_TYPE_3).readNext();
			Assert.assertTrue("Type not found or invalid", FactoryDataFile.getInstance().checkType(row3));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test if return of Factory is instance of DomainSalesman.
	 * {@link DomainSalesman}
	 */
	@Test
	public void testGetDomainSalesman() {

		DomainDataFile decorator = null;
		
		try {
			
			String[] row = ImportFile.getCSVReader(FILE_DIR_TYPE_1).readNext();
			decorator = FactoryDataFile.getInstance().getDomain(row);

			Assert.assertTrue("Factory do not return a DomainSalesman", decorator instanceof DomainSalesman);			
			
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test if return of Factory is instance of DomainCostumer.
	 * {@link DomainCostumer}
	 */
	@Test
	public void testGetDomainCostumer() {

		DomainDataFile decorator = null;
		
		try {
			
			String[] row = ImportFile.getCSVReader(FILE_DIR_TYPE_2).readNext();
			decorator = FactoryDataFile.getInstance().getDomain(row);

			Assert.assertTrue("Factory do not return a DomainCostumer", decorator instanceof DomainCostumer);			
			
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test if return of Factory is instance of DomainSales.
	 * {@link DomainSales}
	 */
	@Test
	public void testGetDomainSales() {

		DomainDataFile decorator = null;
		
		try {
			
			String[] row = ImportFile.getCSVReader(FILE_DIR_TYPE_3).readNext();
			decorator = FactoryDataFile.getInstance().getDomain(row);

			Assert.assertTrue("Factory do not return a DomainSales", decorator instanceof DomainSales);			
			
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
