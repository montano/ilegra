package com.ilegra.core;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.domain.DomainSalesman;

public class ReadDataFileTest {

	private ReadDataFile instance;

	@Before
	public void setUp() throws Exception {
		instance = new ReadDataFile();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	/**
	 * Test if ReadDataFileRow can read with success, a Salesman type row.
	 */
	@Test
	public void testReadRowSalesman() {
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_1).readNext();
			
			DomainSalesman domainSalesman = (DomainSalesman) instance.readRow(row);
			Assert.assertNotNull("Read Salesman row fail", domainSalesman);			
			
			if (domainSalesman == null) {
				return;
			}
			
			Assert.assertEquals("Error on assert salesman name", "Diego", domainSalesman.getName());
			Assert.assertEquals("Error on assert salesman cpf", "1234567891234", domainSalesman.getCpf());
			Assert.assertEquals("Error on assert salesman salary", 50000D, domainSalesman.getSalary());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
