package com.ilegra.domain;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.core.FactoryDataFileTest;
import com.ilegra.core.ImportFile;

public class DomainItemTest {

	private DomainItem instance;

	@Before
	public void setUp() throws Exception {
		instance = new DomainItem();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testParse() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_ITEM, DomainItem.DELIMETER).readNext();
			instance.parse(row);
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheck() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_ITEM, DomainItem.DELIMETER).readNext();
			Assert.assertTrue("Error on check values of DomainItem", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckFail() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_ITEM, DomainItem.DELIMETER).readNext();
			
			/**
			 * Change values
			 */
			row[1] = null;
			row[2] = null;
			
			Assert.assertFalse("Error on check values of DomainItem", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
