package com.ilegra.domain;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.core.FactoryDataFileTest;
import com.ilegra.core.ImportFile;

public class DomainCostumerTest {

	private DomainCostumer instance;

	@Before
	public void setUp() throws Exception {
		instance = new DomainCostumer();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testParse() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_2).readNext();
			instance.parse(row);
			
			Assert.assertEquals("Error on assert salesman name", "Jose da Silva", instance.getName());
			Assert.assertEquals("Error on assert salesman cpf", "2345675434544345", instance.getCnpj());
			Assert.assertEquals("Error on assert salesman salary", "Rural", instance.getBusiness());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheck() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_2).readNext();
			Assert.assertTrue("Error on check values of DomainCostumer", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckFail() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_2).readNext();
			
			/**
			 * Change values
			 */
			row[3] = null;
			row[2] = null;
			
			Assert.assertFalse("Error on check values of DomainCostumer", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
