package com.ilegra.domain;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.core.FactoryDataFile;

public class DomainResultsTest {

	private DomainResults instance;

	@Before
	public void setUp() throws Exception {
		instance = new DomainResults();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testDone() {
		
		try {
			
			DomainCostumer costumer = new DomainCostumer();
			costumer.setType(FactoryDataFile.TYPE_COSTUMER);
			instance.done(costumer);
			
			Assert.assertEquals(1, instance.getSumCostumers());
			
			DomainSalesman salesman = new DomainSalesman();
			salesman.setType(FactoryDataFile.TYPE_SALESMAN);
			instance.done(salesman);
			instance.done(salesman);
			
			Assert.assertEquals(2, instance.getSumSalesman());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}	
}
