package com.ilegra.domain;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.core.FactoryDataFileTest;
import com.ilegra.core.ImportFile;

public class DomainSalesmanTest {

	private DomainSalesman instance;

	@Before
	public void setUp() throws Exception {
		instance = new DomainSalesman();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testParse() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_1).readNext();
			instance.parse(row);
			
			Assert.assertEquals("Error on assert salesman name", "Diego", instance.getName());
			Assert.assertEquals("Error on assert salesman cpf", "1234567891234", instance.getCpf());
			Assert.assertEquals("Error on assert salesman salary", 50000D, instance.getSalary());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheck() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_1).readNext();
			Assert.assertTrue("Error on check values of DomainSalesman", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckFail() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_1).readNext();
			
			/**
			 * Change values
			 */
			row[3] = null;
			row[2] = null;
			
			Assert.assertFalse("Error on check values of DomainSalesman", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
