package com.ilegra.domain;

import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ilegra.core.FactoryDataFileTest;
import com.ilegra.core.ImportFile;

public class DomainSalesTest {

	private DomainSales instance;

	@Before
	public void setUp() throws Exception {
		instance = new DomainSales();
	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testParse() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_3).readNext();
			instance.parse(row);
			
			Assert.assertTrue("Error salesman", instance.getSalesman() instanceof DomainSalesman);
			Assert.assertTrue("Error no itens", instance.getItens().size() > 0);
			
			DomainItem firstItem = instance.getItens().get(0);
			Assert.assertEquals("Error on assert item id", 1, firstItem.getId());
			Assert.assertEquals("Error on assert item number", 10, firstItem.getNumber());
			Assert.assertEquals("Error on assert item price", 100D, firstItem.getPrice());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testTotalSales() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_3).readNext();
			instance.parse(row);
			Assert.assertTrue("Total should be 1199!",instance.getTotalSales() == 1199);
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheck() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_3).readNext();
			Assert.assertTrue("Error on check values of DomainSales", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckFail() {
		
		try {
			
			String[] row = ImportFile.getCSVReader(FactoryDataFileTest.FILE_DIR_TYPE_3).readNext();
			
			/**
			 * Change values
			 */
			row[1] = null;
			row[2] = null;
			
			Assert.assertFalse("Error on check values of DomainSales", instance.checkValues(row));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
